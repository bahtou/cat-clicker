(function() {

  var model = {
    currentCat: null,
    cats: [
      {
        name: 'Batcat',
        src: {
          name: 'batman.jpg',
          width: 286,
          height: 400
        },
        clickCount: 0
      },
      {
        name: 'Supercat',
        src: {
          name: 'superman.jpg',
          width: 266,
          height: 400
        },
        clickCount: 0
      },
      {
        name: 'Shameless',
        src: {
          name: 'shame.jpg',
          width: 373,
          height: 400
        },
        clickCount: 0
      },
      {
        name: 'Cat Business',
        src: {
          name: 'business.jpg',
          width: 544,
          height: 400
        },
        clickCount: 0
      },
      {
        name: 'Cat Vader',
        src: {
          name: 'darthvader.jpg',
          width: 400,
          height: 400
        },
        clickCount: 0
      },
      {
        name: 'IT Cat',
        src: {
          name: 'IT.jpg',
          width: 394,
          height: 400
        },
        clickCount: 0
      }
    ]
  };

  var octopus = {
    incrementCatCount: function() {
      model.currentCat.clickCount++;
      views.catView.render(model.currentCat);
    },
    getCurrentCat: function() {
      return model.currentCat;
    },
    getCats: function() {
      return model.cats;
    },
    setCurrentCat: function(cat) {
      model.currentCat = cat;
    },
    init: function() {
      model.currentCat = model.cats[0];

      views.catList.init();
      views.catView.init();
      views.catAdmin.init();
    }
  };

  var views = {
    catList: {
      init: function() {
        this.catItems = document.getElementsByClassName('cat-items')[0];

        //event listner after the rendering
        views.catList.render();
      },
      render: function() {
        var cats = octopus.getCats();
        var template = '';

        this.catItems.innerHTML = '';

        cats.forEach(function(cat) {
          template += '<li class="cat-item">' +
            cat.name +
            '</li>';
        });

        this.catItems.innerHTML = template;

        this.catItems.addEventListener('click', function(event) {
          var catName;

          if (event.target.className === 'cat-item') {
            catName = event.target.textContent;

            cats.forEach(function(cat) {
              if (cat.name === catName) {
                octopus.setCurrentCat(cat);
                views.catView.render(cat);
              }
            });
          }
        }, false);
      }
    },
    catView: {
      init: function() {
        var initCat = octopus.getCurrentCat();

        this.catName = document.getElementsByClassName('cat-name')[0];
        this.catImage = document.getElementsByClassName('cat-image')[0];
        this.catCounter = document.getElementsByClassName('cat-counter')[0];

        this.catImage.addEventListener('click', function(event) {
            octopus.incrementCatCount();
        }, false);

        views.catView.render(initCat);
      },
      render: function(cat) {
        this.catName.textContent = cat.name;
        this.catImage.src = 'images/' + cat.src.name;
        this.catImage.alt = cat.name;
        this.catImage.width = cat.src.width;
        this.catImage.height = cat.src.height;
        this.catCounter.textContent = cat.clickCount;
      }
    },
    catAdmin: {
      init: function() {
        var editButton = this.editButton = document.getElementsByClassName('cat-admin__edit-button')[0];
        var cancelButton = this.cancelButton =  document.getElementsByClassName('cat-admin__cancel-button')[0];
        var saveButton = this.saveButton =  document.getElementsByClassName('cat-admin__save-button')[0];
        var adminEditor = this.adminEditor = document.getElementsByClassName('cat-admin__editor')[0];

        var adminName = this.adminName = document.getElementsByClassName('cat-admin__name')[0];
        var adminUrl = this.adminUrl = document.getElementsByClassName('cat-admin__url')[0];
        var adminCounter = this.adminCounter = document.getElementsByClassName('cat-admin__counter')[0];

        editButton.addEventListener('click', function(event) {
          editButton.setAttribute('disabled', '');
          adminEditor.classList.remove('cat-admin__editor--hidden');

          views.catAdmin.render();
        }, false);

        cancelButton.addEventListener('click', function(event) {
          editButton.removeAttribute('disabled', '');
          adminEditor.classList.add('cat-admin__editor--hidden');
        }, false);

        saveButton.addEventListener('click', function(event) {
          var cat = octopus.getCurrentCat();

          cat.name = adminName.value;
          cat.src.name = adminUrl.value;
          cat.clickCount = adminCounter.value;

          views.catList.render();
          views.catView.render(cat);

          editButton.removeAttribute('disabled', '');
          adminEditor.classList.add('cat-admin__editor--hidden');

        }, false);
      },
      render: function() {
        var cat = octopus.getCurrentCat();

        this.adminName.value = cat.name;
        this.adminUrl.value = cat.src.name;
        this.adminCounter.value = cat.clickCount;
      }
    }
  };

  document.addEventListener('DOMContentLoaded', octopus.init);

})();



